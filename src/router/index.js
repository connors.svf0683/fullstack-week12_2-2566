import { createRouter,createWebHistory } from "vue-router";

const routes = [
    {
        path: '/',
        name: 'home',
        component: ()=> import('../views/HomeView.vue')
    },
    {
        path: '/about',
        name: 'about',
        component: ()=> import('../views/AboutView.vue')
    },
    {
        path: '/user',
        name: 'user',
        component: ()=> import('@/views/UserView.vue')
    },
    {
        path: '/user/:id',
        name: 'UserSingle',
        component: ()=> import('@/views/UserSinglePage.vue'),
        props: true,
    },
    {
        path: '/member',
        redirect: '/about',
    },
    {
        path: "/:catchAll(.*)*",
        name: 'NotFound',
        component: ()=> import('../views/NotFound.vue')
    }
]
const router = createRouter({
    history : createWebHistory(),
    routes
});

export default router